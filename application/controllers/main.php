<?php

class main extends CI_Controller{
    function __construct() 
    {	
        parent::__construct();        
        $this->templatee = "site";
        $this->load->model("main_m");
        $this->load->library('Datatables');
        // $this->load->library('table');
        // $this->load->database();
    }
    
    function index()
    { 
        $result = $this->main_m->get_data_antrian();
        $data['count_kp'] = $this->main_m->count_kp();
        $data['count_skripsi'] = $this->main_m->count_skripsi();
        $data['count_survey'] = $this->main_m->count_survey();
        $data['count_sk'] = $this->main_m->count_sk();
        if (isset($result)) {
            # code...
            $data['query'] = $result;
            $this->load->view('main', $data);
        } else {
            $this->load->view('main', $data);
        }

        // $tmpl = array('table_open' => '<table id="example" class="table table-vcenter table-condensed table-bordered table-striped">');
        // $this->table->set_template($tmpl);
 
        // $this->table->set_heading('Kode Antrian', 'No Surat', 'Npm', 'nama', 'keperluan');
        
        //  $this->datatables->select('id,username');
         
        //     $this->datatables->from('admin');
 
        // // $result = $this->datatables->generate();
        // // $data['user'] = $result['username'];
        // // foreach ($data as $key) {
        // //     # code...
        // //     echo $key->username;
        // // }

        // $this->load->view('main2');
    }
    function submit(){
        //load model antrian
        $this->load->model('antrian');
        // get result from antrian model
        $result = $this->antrian->submit_surat();

        //back to main page
        $this->index();
    }
    function view_antrian(){
    	$result = $this->main_m->get_data_antrian();
    	if (isset($result)) {
    		# code...
    		$data['query'] = $result;
    		$this->load->view('main', $data);
    	} else {
    		redirect('main','refresh');
    	}
    }
    function view_surat(){
        $result = $this->main_m->get_data_surat();
        if (isset($result)) {
            # code...
            $data['query'] = $result;
            $this->load->view('surat', $data);
        } else {
            redirect('main','refresh');
        }
    }
}
