<?php

class main extends CI_Controller{
    function __construct() 
    {	
        parent::__construct();        
        $this->templatee = "site";
        $this->load->model("main_m");
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->database();
    }
    
    function index()
    { 
        $result = $this->main_m->get_data_antrian();
        $data['count_kp'] = $this->main_m->count_kp();
        $data['count_skripsi'] = $this->main_m->count_skripsi();
        $data['count_survey'] = $this->main_m->count_survey();
        $data['count_sk'] = $this->main_m->count_sk();
        if (isset($result)) {
            # code...
            $data['query'] = $result;
            $this->load->view('main', $data);
        } else {
            $this->load->view('main', $data);
        }

        // $tmpl = array('table_open' => '<table id="example" class="table table-vcenter table-condensed table-bordered table-striped">');
        // $this->table->set_template($tmpl);
 
        // $this->table->set_heading('Kode Antrian', 'No Surat', 'Npm', 'nama', 'keperluan');
        
        //  $this->datatables->select('id,username');
         
        //     $this->datatables->from('admin');
 
        // // $result = $this->datatables->generate();
        // // $data['user'] = $result['username'];
        // // foreach ($data as $key) {
        // //     # code...
        // //     echo $key->username;
        // // }

        // $this->load->view('main2');
    }
    function submit(){
        //load model antrian
        $this->load->model('antrian');

        // get result from antrian model
        $result = $this->antrian->submit_surat();

        //back to main page
        $this->index();

    }
    function datatable(){
        $this->datatables->select('id_antrian,no_surat,npm,nama,jenis_surat')
        // ->unset_column('id')
            ->from('view_antrian_mhs');
 
        echo $this->datatables->generate();
    }

//     public function ajax_tabel()
//     {
//         if (!$this->input->is_ajax_request()) {
//             exit('No direct script access allowed');
//         } else {
// //            panggil dulu library datatablesnya
            
//             $this->load->library('datatables_ssp');
            
// //            atur nama tablenya disini
//             $table = 'view_antrian_mhs';
 
//             // Table's primary key
//             $primaryKey = 'id_antrian';
 
//             // Array of database columns which should be read and sent back to DataTables.
//             // The `db` parameter represents the column name in the database, while the `dt`
//             // parameter represents the DataTables column identifier. In this case simple
//             // indexes
 
//             $columns = array(
//                 array('db' => 'id_kodepos', 'dt' => 'DT_RowId'),
//                 array('db' => 'kodepos', 'dt' => 'kodepos'),
//                 array('db' => 'kelurahan', 'dt' => 'kelurahan'),
//                 array('db' => 'kecamatan', 'dt' => 'kecamatan'),
//                 array('db' => 'kota', 'dt' => 'kota'),
//                 array('db' => 'provinsi', 'dt' => 'provinsi'),
//                 array(
//                     'db' => 'id_kodepos',
//                     'dt' => 'aksi',
//                     'formatter' => function( $d ) {
//                         return '<a href="' . site_url('kodepos/update/' . $d) . '">Edit</a> | <a href="' . site_url('kodepos/delete/' . $d) . '">Delete</a>';
//                     }
//                 ),
//             );
 
//             // SQL server connection information
//             $sql_details = array(
//                 'user' => 'root',
//                 'pass' => '',
//                 'db' => 'harviacode',
//                 'host' => 'localhost'
//             );
 
//             echo json_encode(
//                     Datatables_ssp::simple($_GET, $sql_details, $table, $primaryKey, $columns)
//             );
//         }
    function view_antrian(){


    	$result = $this->main_m->get_data_antrian();
    	if (isset($result)) {
    		# code...
    		$data['query'] = $result;
    		$this->load->view('main', $data);
    	} else {
    		redirect('main','refresh');
    	}
    }
	
}
