<?php

class word extends CI_Controller{
    function __construct() 
    {	
        parent::__construct();        
        $this->templatee = "site";
        $this->load->library('word');
       // $this->load->library('session');
        $this->load->model('process_m');
        setlocale(LC_ALL, 'IND');
    }
    
  /*  function index()
    { 	
      $this->load->library('word');
      //our docx will have 'lanscape' paper orientation
      $section = $this->phpword->createSection(array('orientation'=>'landscape'));
      // Add text elements
        $section->addText('Hello World!');
        $section->addTextBreak(1);
            
        $section->addText('I am inline styled.', array('name'=>'Verdana', 'color'=>'006699'));
        $section->addTextBreak(1);
            
        $this->word->addFontStyle('rStyle', array('bold'=>true, 'italic'=>true, 'size'=>16));
        $this->word->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
        $section->addText('I am styled by two style definitions.', 'rStyle', 'pStyle');
        $section->addText('I have only a paragraph style definition.', null, 'pStyle');
        // Add image elements
        $section->addImage(FCPATH.'/image/_mars.jpg');
        $section->addTextBreak(1);
            
        $section->addImage(FCPATH.'/image/_earth.JPG', array('width'=>210, 'height'=>210, 'align'=>'center'));
        $section->addTextBreak(1);
            
        $section->addImage(FCPATH.'/image/_mars.jpg', array('width'=>100, 'height'=>100, 'align'=>'right'));
        // Define table style arrays
        $styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>80);
        $styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');
            
        // Define cell style arrays
        $styleCell = array('valign'=>'center');
        $styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);
            
        // Define font style for first row
        $fontStyle = array('bold'=>true, 'align'=>'center');
            
        // Add table style
        $this->word->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
            
        // Add table
        $table = $section->addTable('myOwnTableStyle');
            
        // Add row
        $table->addRow(900);
            
        // Add cells
        $table->addCell(2000, $styleCell)->addText('Row 1', $fontStyle);
        $table->addCell(2000, $styleCell)->addText('Row 2', $fontStyle);
        $table->addCell(2000, $styleCell)->addText('Row 3', $fontStyle);
        $table->addCell(2000, $styleCell)->addText('Row 4', $fontStyle);
        $table->addCell(500, $styleCellBTLR)->addText('Row 5', $fontStyle);
            
        // Add more rows / cells
        for($i = 1; $i <= 2; $i++) {
          $table->addRow();
          $table->addCell(2000)->addText("Cell $i");
          $table->addCell(2000)->addText("Cell $i");
          $table->addCell(2000)->addText("Cell $i");
          $table->addCell(2000)->addText("Cell $i");
              
          $text = ($i % 2 == 0) ? 'X' : '';
          $table->addCell(500)->addText($text);
        }
        $filename='just_some_random_name.docx'; //save our document as this file name
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPWord_IOFactory::createWriter($this->word, 'Word2007');
        $objWriter->save('php://output');
    }
     public function template(){
        //$id = $this->input->GET('id');
      //$id = 
        //$result = $this->process_m->kp($id);
        $this->load->library('PHPWord');
        $document = $this->phpword->loadTemplate('application/docs/temp/Template.docx');
       // $document->setValue('myReplacedValue',$result['pemohon']);
       // $document->setValue('no', $result['nomor']);
        $document->setValue('Value2', 'Mercury');
        $document->setValue('Value3', 'Venus');
        $document->setValue('Value4', 'Earth');
        $document->setValue('Value5', 'Mars');
        $document->setValue('Value6', 'Jupiter');
        $document->setValue('Value7', 'Saturn');
        $document->setValue('Value8', 'Uranus');
        $document->setValue('Value9', 'Neptun');
        $document->setValue('Value10', 'Pluto');

         
        
        //$filename = 'application/controller/Solarsystem.docx';
        $document->setValue('weekday', date('l'));
        $document->setValue('time', date('H:i'));

        $document->setValue('Name', 'bagus');
        $document->setValue('npm', '6040');

        $document->save('application/docs/Solarsystem.docx');

       // header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
       // header('Content-Disposition: attachment;filename="Solarsystem.docx"'); //tell browser what's the file name
       // header('Cache-Control: max-age=0'); //no cache
       // $objWriter = PHPWord_IOFactory::createWriter($this->word, 'Word2007');
       // $objWriter->save('php://output');
    }
*/
    function test(){

      $this->load->library('word');
      
      require_once APPPATH.'/libraries/PhpOffice/PHPWord/src/PhpWord/Autoloader.php';
      PhpOffice\PhpWord\Autoloader::register();
      

      // Template processor instance creation
      echo date('H:i:s') , ' Creating new TemplateProcessor instance...' ;
      $doc = new \PhpOffice\PhpWord\TemplateProcessor('application/docs/temp/Template.docx');


      $doc->setValue('no','123');
       $doc->setValue('Value2', 'Mercury');
        $doc->setValue('Value3', 'Venus');
        $doc->setValue('Value4', 'Earth');
        $doc->setValue('Value5', 'Mars');
        $doc->setValue('Value6', 'Jupiter');
        $doc->setValue('Value7', 'Saturn');
        $doc->setValue('Value8', 'Uranus');
        $doc->setValue('Value9', 'Neptun');
        $doc->setValue('Value10', 'Pluto');

         
        
        //$filename = 'application/controller/Solarsystem.docx';
        $doc->setValue('weekday', date('l'));
        $doc->setValue('time', date('H:i'));

        $doc->setValue('Name', 'bagus');
        $doc->setValue('npm', '6040');

      echo date('H:i:s'), ' Saving the result document...';
      $doc->saveAs('test1.docx');

      $filename='test1.docx'; //save our document as this file name
      header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      $objWriter = PHPWord_IOFactory::createWriter($doc, 'Word2007');
      $objWriter->save('php://output');
          }
    

    function kp(){
      setlocale (LC_TIME, 'id_ID');
      $date = strftime( "%d %B %Y");
      //echo "Saat ini: ".$date;
      $id = $this->input->GET('id');
      //$id = $this->input->GET('');
      $result = $this->process_m->kp($id);

      $this->load->library('word');
      
      require_once APPPATH.'/libraries/PhpOffice/PHPWord/src/PhpWord/Autoloader.php';
      PhpOffice\PhpWord\Autoloader::register();
      

      // Template processor instance creation
      //echo date('H:i:s') , ' Creating new TemplateProcessor instance...' ;
      $doc = new \PhpOffice\PhpWord\TemplateProcessor('application/docs/temp/KP.docx');

      // add your code here .......
      $doc->setValue('nomor', $result['nomor']);

      $doc->setValue('pemohon', $result['npm'][0]);
      $doc->setValue('nama', $result['nama'][0]);
      
      $doc->setValue('tujuan', $result['tujuan']);
      $doc->setValue('tempat', $result['tempat']);
      
      $doc->setValue('npm1',  $result['npm'][1]   );
      $doc->setValue('nama1', $result['nama'][1]  );
      $doc->setValue('npm2',  $result['npm'][2]   );
      $doc->setValue('nama2', $result['nama'][2]  );
      $doc->setValue('npm3',  $result['npm'][3]   );
      $doc->setValue('nama3', $result['nama'][3]  );
      $doc->setValue('npm4',  $result['npm'][4]   );
      $doc->setValue('nama4', $result['nama'][4]  );
      
      $doc->setValue('materi1', $result['materi'][0]);
      $doc->setValue('materi2', $result['materi'][1]);
      $doc->setValue('materi3', $result['materi'][2]);
      $doc->setValue('materi4', $result['materi'][3]);
      $doc->setValue('materi5', $result['materi'][4]);
      //$doc->cloneRow('npm', 5);
      //$i=0;
      //$doc->setValue('npm#1', $result['npm'][0]);
      //$doc->setValue('npm#2', $result['npm'][1]);
      //$doc->setValue('npm#3', $result['npm'][2]);
      //$doc->setValue('npm#4', $result['npm'][3]);
     // $doc->setValue('npm#5', $result['npm'][4]);
//
      
     // $doc->cloneRow('nama', count($result['nama']));
      //$doc->cloneRow('materi', count($result['materi']));

      $doc->setValue('tanggal',$date);
      //echo date('H:i:s'), ' Saving the result document...';
      $doc->saveAs('application/docs/KP/'.$id.$date.'.doc');
      //redirect('main','refresh');
    }
}

	