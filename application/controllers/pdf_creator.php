<?php

class pdf_creator extends CI_Controller{
    function __construct() 
    {	
        parent::__construct();        
        $this->templatee = "site";
    }
    
    function index()
    { 
		$filename = 'testing';
       
		// As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
		$pdfFilePath = FCPATH."$filename.pdf";
		//$data['page_title'] = 'Hello world'; // pass data to the view
		 
		if (file_exists($pdfFilePath) == FALSE)
		{
			ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://s.w.org/images/core/emoji/72x72/1f609.png" alt="😉" draggable="false" class="emoji">
			//$html = $this->load->view('dashboard'); // render the view into HTML
			 
			$this->load->library('pdf');
			$pdf = $this->pdf->load();
			
			$pdf->SetImportUse();
			//$pdf->setHTMLHeader();
			$sourceTemplate = $pdf->SetSourceFile(FCPATH."assets/template/header&footer.pdf");
			$templateHeader = $pdf->ImportPage($sourceTemplate, 1);
			$templateFooter = $pdf->ImportPage($sourceTemplate, 2);

			//$pdf->SetPageTemplate($templateHeader);

			$header = $pdf->UseTemplate($templateHeader);
			$footer = $pdf->UseTemplate($templateFooter);
			$pdf->SetHeader($header);
			$pdf->SetFooter($footer);

			//$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://s.w.org/images/core/emoji/72x72/1f609.png" alt="😉" draggable="false" class="emoji">
			//$pdf->WriteHTML("hello bagus"); // write the HTML into the PDF
			$pdf->Output($pdfFilePath, 'F'); // save to file because we can
		}
		 
		redirect("$filename.pdf");
		
    }
	
}
