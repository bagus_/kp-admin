<?php $this->load->view('template/sidebar'); ?>
			 <!-- Page content -->
        <div id="page-content">
				
			        <!-- Dashboard Header -->
                    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
                    <div class="content-header content-header-media">
                        <div class="header-section">
                            <div class="row text-center">
                                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h1> <strong>Selamat Datang </strong>Admin<br></h1>
                                </div>
                                <!-- END Main Title -->
                            </div>
                        </div>
                        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
                        <img src="<?php echo base_url(); ?>assets/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
                    </div>
                    <!-- END Dashboard Header -->

                    <!-- Mini Top Stats Row -->
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="<?=base_url()?>assets/template/templateKP.pdf" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                                        <i class="fa fa-file-text"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        + <strong><?=$count_kp ?></strong><br>
                                        <small>Permohonan Kerja praktek</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="page_comp_charts.html" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                                        <i class="gi gi-usd"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        + <strong><?=$count_skripsi ?></strong><br>
                                        <small>Permohonan Skripsi</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="page_ready_inbox.html" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                                        <i class="gi gi-envelope"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        + <strong><?=$count_survey ?></strong>
                                        <small>permohonan Survey</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="page_comp_gallery.html" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                                        <i class="gi gi-picture"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        + <strong><?=$count_sk ?></strong>
                                        <small>Surat Keterangan Mahasiswa</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
						 <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="page_comp_gallery.html" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                                        <i class="gi gi-picture"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        +30 <strong>Photos</strong>
                                        <small>Permohonan perpanjangan Skripsi</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
						 <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="page_comp_gallery.html" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                                        <i class="gi gi-picture"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        +30 <strong>Photos</strong>
                                        <small>Permohonan perpanjangan Skripsi</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
						 <div class="col-sm-6 col-lg-3">
                            <!-- Widget -->
                            <a href="page_comp_gallery.html" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                                        <i class="gi gi-picture"></i>
                                    </div>
                                    <h3 class="widget-content text-right animation-pullDown">
                                        +30 <strong>Photos</strong>
                                        <small>Permohonan perpanjangan Skripsi</small>
                                    </h3>
                                </div>
                            </a>
                            <!-- END Widget -->
                        </div>
                    </div>
                    <!-- END Mini Top Stats Row -->
                    <br><br>
            <!-- Datatables Header -->
                    <div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
                            </h1>
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Tables</li>
                        <li><a href="">Datatables</a></li>
                    </ul>
                    <!-- END Datatables Header -->
                    
                    <!-- Datatables Content -->
                   <div class="row">
                         <div class="block full">
                        <div class="block-title">
                            <h2><strong>Datatables</strong> integration</h2>
                        </div>
                        <div class="table-responsive">
                            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered table-striped">
                                <thead>
                                    <tr>
                                         <th class="text-center"> kode antrian </th>
                                        <th class="text-center"> No. Surat </th>
                                        <th class="text-center"> NPM </th>
                                        <th class="text-center"> Nama </th>
                                        <th class="text-center"> Keperluan </th>
                                        <th class="text-center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                               // $no = 1;
                                foreach ($query->result() as $item):?>
                                <tr>
                                    <td class="text-center"><?= $item->id_antrian;?></td>
                                    <td class="text-center"><?= $item->no_surat;?></td>
                                    <td class="text-center"><?= $item->npm;?></td>
                                    <td><?= $item->nama;?></td>
                                    <td class="text-center"><?= $item->jenis_surat;?></td>
                                    <td class="text-center">
                                            <div class="btn-group">
                                                <a href="<?= base_url(); ?>main/submit/nomor<?= $item->no_surat; ?>" data-toggle="tooltip" title="Aprove" class="btn btn-lg btn-warning">terima</i></a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Aprove" class="btn btn-lg btn-success disabled"><i class="gi gi-ok_2"></i></a>
                                                <a href="<?= base_url(); ?>word/<?= $item->jenis_surat;?>?id=<?= $item->id_antrian;?>" data-toggle="modal" class="btn btn-lg btn-primary">Cetak</i></a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-lg btn-danger"><i class="gi gi-remove_2"></i></a>
                                            </div>
                                    </td>
                                </tr>
                                <?php
                                    //$no++;
                                endforeach;?>
                                </tbody>
                             </table>
                        </div>                    
                    </div>
                   </div>
                    <!-- END Datatables Content -->

		</div>
        <!-- END Page Content -->


        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
                            <fieldset>
                                <legend>Vital Info</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Username</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static">Admin</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                                    <div class="col-md-8">
                                        <label class="switch switch-primary">
                                            <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->
<?php $this->load->view('template/footbar'); ?>